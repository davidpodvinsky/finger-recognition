import webbrowser
import subprocess
import os
import ctypes
from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume
import cv2
import pyautogui



openYoutube = """
# open youtube
url = "https://www.youtube.com/"
webbrowser.open(url)
"""

openGoogle = """
# open google
url = "http://www.google.com/"
webbrowser.open(url)
"""

openChanel9 = """
# open chanel 9
url = "https://www.9now.com.au/"
webbrowser.open(url)
"""

openYnet = """
# open ynet
url = "https://www.ynet.co.il/home/0,7340,L-8,00.html"
webbrowser.open(url)
"""

openWord = """
# open word
subprocess.Popen(["start", "winword.exe"], shell=True)
"""

openPaint = """
# open paint
subprocess.Popen(["start", "mspaint.exe"], shell=True)
"""

openExcel = """
# open excel
subprocess.Popen(["start", "excel.exe"], shell=True)
"""

openSettings = """
# open settings
subprocess.Popen(["start", "ms-settings:"], shell=True)
"""

muteSound = """
# mute sound
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
volume.SetMute(1, None)
"""

unmuteSound = """
# unmute sound
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
volume.SetMute(0, None)
"""

takeScreenshot = """
# take screenshot
screenshot = pyautogui.screenshot()
screenshot.save('screenshot.png')
"""

decreaseVolume = """
# decrease volume 10
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
current = volume.GetMasterVolumeLevel()
if current > -1.5:
    volume.SetMasterVolumeLevel(-1.5, None)     # 90
elif current > -3:
    volume.SetMasterVolumeLevel(-3.3, None)     # 80
elif current > -5:
    volume.SetMasterVolumeLevel(-5.3, None)     # 70
elif current > -7:
    volume.SetMasterVolumeLevel(-7.6, None)     # 60
elif current > -10:
    volume.SetMasterVolumeLevel(-10.2, None)     # 50
elif current > -13:
    volume.SetMasterVolumeLevel(-13.8, None)     # 40
elif current > -18:
    volume.SetMasterVolumeLevel(-18, None)     # 30
elif current > -23:
    volume.SetMasterVolumeLevel(-23.7, None)     # 20
elif current > -33:
    volume.SetMasterVolumeLevel(-33.7, None)     # 10
elif current > -65:
    volume.SetMasterVolumeLevel(-65.0, None)     # 0
"""

increaseVolume = """
#increase volume 10
devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))
current = volume.GetMasterVolumeLevel()
if current > -3:
    volume.SetMasterVolumeLevel(0.0, None)     # 100
elif current > -5:
    volume.SetMasterVolumeLevel(-1.5, None)     # 90
elif current > -7:
    volume.SetMasterVolumeLevel(-3.3, None)     # 80
elif current > -10:
    volume.SetMasterVolumeLevel(-5.3, None)     # 70
elif current > -13:
    volume.SetMasterVolumeLevel(-7.6, None)     # 60
elif current > -18:
    volume.SetMasterVolumeLevel(-10.2, None)     # 50
elif current > -23:
    volume.SetMasterVolumeLevel(-13.8, None)     # 40
elif current > -33:
    volume.SetMasterVolumeLevel(-18, None)     # 30
elif current > -65:
    volume.SetMasterVolumeLevel(-23.7, None)     # 20
elif current > -70:
    volume.SetMute(0, None)
    volume.SetMasterVolumeLevel(-33.7, None)     # 10
"""

closeWord = """
# close word
os.system("taskkill /f /im winword.exe > nul")
"""

closeExcel = """
# close excel
os.system("taskkill /f /im excel.exe > nul")
"""

closePaint = """
# close paint
os.system("taskkill /f /im mspaint.exe > nul")
"""
