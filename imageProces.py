import cv2


def countNumberOfRaisedFingers(handLabel, handLandmarks, fingerCount):
  # Test conditions for each finger: Count is increased if finger is
  #   considered raised.
  # Thumb: TIP x position must be greater or lower than IP x position,
  #   deppeding on hand label.


  #in the next sector we compare the y of the fingers
  # Other fingers: TIP y position must be lower than PIP y position,
  #   as image origin is in the upper left corner.

  result=""
  if handLandmarks[20][1] < handLandmarks[18][1]:     #Pinky
    fingerCount = fingerCount+1
    result+="1"
  else:
    result+="0"

  if handLandmarks[16][1] < handLandmarks[14][1]:     #Ring finger
    fingerCount = fingerCount+1
    result+="1"
  else:
    result+="0"

  if handLandmarks[12][1] < handLandmarks[10][1]:     #Middle finger
    fingerCount = fingerCount+1
    result+="1"
  else:
    result+="0"

  if handLandmarks[8][1] < handLandmarks[6][1]:       #Index finger  [8][1] = 8[1]
    fingerCount = fingerCount+1
    result+="1"
  else:
    result+="0"

  if handLabel == "Left":#if x of point 4 is above x of point 3 from the left add 1
    if handLandmarks[4][0] > handLandmarks[3][0]:
      fingerCount = fingerCount+1
      result+="1"
    else:
      result+="0"
  elif handLabel == "right":#if x of point 4 is above x of point 3 from the left add 1
    if handLandmarks[4][0] < handLandmarks[3][0]:
      fingerCount = fingerCount+1
      result+="1"
    else:
      result+="0"
  else:
    result+="0"
    #we compare the x of the points because the thumb is laying on the side compared to other fingers
  #we just check that every point is above the lower one
  return fingerCount,result



def improvePerformanceOfPic(image, hands):
  # To improve performance, optionally mark the image as not writeable to
  # pass by reference.
  image.flags.writeable = False
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
  results = hands.process(image)

  # Draw the hand annotations on the image.
  image.flags.writeable = True
  image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
  return image, results



def makeArrWithPoisitions(results):
  handLandmarksArr = []
  try:
    hand_landmarks = results.multi_hand_landmarks[0]
  except:
    print(results.multi_hand_landmarks)
  # Get hand index to check label (left or right)
  handIndex = results.multi_hand_landmarks.index(hand_landmarks)
  handLabel = results.multi_handedness[handIndex].classification[0].label
  # Set variable to keep landmarks positions (x and y)
  # Fill list with x and y positions of each landmark
  for landmarks in hand_landmarks.landmark:
    handLandmarksArr.append([landmarks.x, landmarks.y])
  return handLandmarksArr, handLabel



'''
claculates the degrees and rotates the image
gets: image to rotate, array of points of landmarks, the direction of the palm
'''
def rotateImageMy(image,handLandmarks,handLabel):
  angleDegrees = calc_degrees(handLandmarks,handLabel)

  cv2.imshow('MediaPipe Hands bad', image)

  print(angleDegrees)

  image=imutils.rotate(image,angle=(angleDegrees))

  return image,angleDegrees
