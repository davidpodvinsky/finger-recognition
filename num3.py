import cv2
import os
import mediapipe as mp
from PyQt5 import uic, QtGui
from PyQt5.QtWidgets import *
import sys
from PyQt5.QtCore import QTimer
import imutils
import math
import numpy as np
from screeninfo import *
from math import *
from meth import *
from imageProces import *
from dataBase import get_command_from_specific_combination
from functionsAsStrings import *
#from menu import userMenu, combinationsMenu
"""
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands
"""
ALLOWANCE_FILE="stop.txt"


def check_run_allowance():
  try:
    file=open(ALLOWANCE_FILE,'r')
    allowance=file.read()
    file.close()
    return "stop" not in allowance
  except:
    return True

def get_most_popular(arr):
  common=max(set(arr), key = arr.count)
  if arr.count(common)>=50:
    return common
  else:
    return None


def displayImage(label,connection,user_id):
  combination_arr=["00000", "01000", "00100", "01100", "00010", "01010", "00110", "01110", "10000", "11000", "10100", "11100", "10010", "11010", "10110", "11110", "00001", "01001", "00101", "01101", "00011", "01011", "00111", "01111", "10001", "11001", "10101", "11101", "10011", "11011", "10111", "11111"]
  combination_dictionary={}

  cur_place=0
  combination_reminder=[]
  for i in range(60):
    combination_reminder.append("fffff")


  for combination in combination_arr:
    combination_dictionary[combination]=get_command_from_specific_combination(connection,combination,user_id)

  mp_drawing = mp.solutions.drawing_utils
  mp_drawing_styles = mp.solutions.drawing_styles
  mp_hands = mp.solutions.hands
  last_combination="fffff"

  #conn = openDataBase()   #open data base or open her and gets the conn to the db
  #connected_user_id = userMenu(conn)
  #combinationsMenu(conn, connected_user_id)
  # For webcam input:
  cap = cv2.VideoCapture(0)
  with mp_hands.Hands(
      static_image_mode=False,
      model_complexity=1,
      min_detection_confidence=0.5,
      min_tracking_confidence=0.5,
      max_num_hands=1) as hands:
    while cap.isOpened():
      success, image = cap.read()
      rotateImage = image
      if not success:
        print("Ignoring empty camera frame.")
        # If loading a video, use 'break' instead of 'continue'.
        continue

      image, results = improvePerformanceOfPic(image, hands)

      # Initially set finger count to 0 for each cap
      fingerCount = 0
      if results.multi_hand_landmarks:
        handLandmarks, handLabel = makeArrWithPoisitions(results)

        dimensions=image.shape#(max y, max x,max z)
        #print(dimensions)
        #image=cv2.circle(image,(round(dimensions[1]*center_point[0]),round(dimensions[0]*center_point[1])),100,(0,0,255),-1)
        #cv2.imshow('check center', image)
        #check if the hand is rotated to any side and rotate her back to the normal place
        back_up=image

        center_point = calc_center_of_palm(handLandmarks)
        angles = calc_degrees(handLandmarks,handLabel)

        updated_handlandmark=[]

        for point in handLandmarks:
          point=rotate_point_around_center(point,-angles,center_point)
          updated_handlandmark.append(point)
          #image=cv2.circle(image,(round(dimensions[1]*point[0]),round(dimensions[0]*point[1])),10,(0,0,255),-1)

        #  if(handLandmarks[5][1]>handLandmarks[0][1]and handLandmarks[17][1]>handLandmarks[0][1]):
        #    image=imutils.rotate(image,angle=180)

          # Draw hand landmarks
          #mp_drawing.draw_landmarks(
           # image,
            #results.multi_hand_landmarks[0],
            #mp_hands.HAND_CONNECTIONS,
            #mp_drawing_styles.get_default_hand_landmarks_style(),
            #mp_drawing_styles.get_default_hand_connections_style())




        fingerCount,cur_combination = countNumberOfRaisedFingers(handLabel,updated_handlandmark,fingerCount)
        combination_reminder[cur_place] = cur_combination
        if cur_place == 59:
          cur_place=0
        else:
          cur_place+=1

        #   just for now check the number of the raised fingers and open aplication according to the number
        popular=get_most_popular(combination_reminder)

        if popular != None and popular != "fffff":
          command=combination_dictionary[popular]
          cur_place=0
          for i in range(60):
            combination_reminder[i] = ("fffff")
          if command:
            exec(command)



      #image=cv2.circle(image,(0,0),100,(0,0,255),-1)
      else:
        combination_reminder[cur_place] = "fffff"
        if cur_place == 59:
          cur_place=0
        else:
          cur_place+=1
      # Display finger count
      cv2.putText(image, str(fingerCount), (50, 450), cv2.FONT_HERSHEY_SIMPLEX, 3, (255, 0, 0), 10)


      #image=imutils.rotate(image,angle=-45)

      
      # Display image
      height1, width1, channel1 = image.shape
      bytesPerLine = 3 * width1
      qImg = QtGui.QImage(image.data, width1, height1, bytesPerLine, QtGui.QImage.Format_RGB888)
      label.setPixmap(QtGui.QPixmap.fromImage(qImg))
      #cv2.imshow('MediaPipe Hands', image)
      if (not check_run_allowance()):
        break

      if cv2.waitKey(5) & 0xFF == 27:
        break
  cap.release()

"""
if __name__ == "__main__":
    main()
"""

