from PyQt5 import uic, QtGui, QtCore
from PyQt5.QtWidgets import *
import sys
from PyQt5.QtCore import QTimer
from dataBase import *
from num3 import *
from menu import *
import cv2
import os
from functionsAsStrings import *
functions_dict = {"open_google": openGoogle, "open_youtube": openYoutube, "open_chanel_9": openChanel9, "open_ynet": openYnet, "open_word": openWord, "open_paint": openPaint, "open_excel": openExcel, "open_settings": openSettings, "mute_sound": muteSound, "unmute_sound": unmuteSound, "increase_volume_10": increaseVolume, "decrease_volume_10": decreaseVolume, "close_word": closeWord, "close_excel": closeExcel, "close_paint": closePaint, "take_screenshot": takeScreenshot}

connectedUserId = 0
conn = None

ID_SAVER="id.txt"
ALLOWANCE_FILE="stop.txt"

def write_allow_run():
    file=open(ALLOWANCE_FILE,'w')
    file.write("run")
    file.close()

def write_stop_run():
    file=open(ALLOWANCE_FILE,'w')
    file.write("stop")
    file.close()


def save_user_id(id):
    id_saver=open(ID_SAVER,"w")
    id_saver.write(str(id))
    id_saver.close()

def get_user_id():
    id_saver=open(ID_SAVER,"r")
    id=int(id_saver.read())
    id_saver.close()
    return id

class MainWindow1(QMainWindow):
    def __init__(self):
        super(MainWindow1, self).__init__()
        write_allow_run()

        self.login = None
        self.register = None

        uic.loadUi("userMenu.ui", self)


        self.loginButton = self.findChild(QPushButton, "loginButton")
        self.registerButton = self.findChild(QPushButton, "registerButton")

        self.loginButton.clicked.connect(self.openLogin)
        self.registerButton.clicked.connect(self.openRegister)

        self.show()

    def openLogin(self):
        if self.login is None:
            self.login = LoginWindow()
            self.close()
        else:
            self.login.activateWindow()

    def openRegister(self):
        if self.register is None:
            self.register = RegisterWindow()
            self.close()
        else:
            self.register.activateWindow()



class LoginWindow(QMainWindow):
    def __init__(self):
        super(LoginWindow, self).__init__()

        self.menu = None
        self.general = None

        uic.loadUi("login.ui", self)

        self.lineEdit_username_login = self.findChild(QLineEdit, "lineEdit_username_login")
        self.lineEdit_password_login = self.findChild(QLineEdit, "lineEdit_password_login")
        self.submitButton_login = self.findChild(QPushButton, "submitButton_login")
        self.error_label_login = self.findChild(QLabel, "error_label_login")
        self.backButton_login = self.findChild(QPushButton, "backButton_login")


        self.error_label_login.setVisible(False)

        self.submitButton_login.clicked.connect(self.checkIfUserExistInDataBase)
        self.backButton_login.clicked.connect(self.openFirstWindow)

        self.show()



    def checkIfUserExistInDataBase(self):
        response = loginMenu(conn, self.lineEdit_username_login.text(), self.lineEdit_password_login.text())
        if response == "name":
            self.error_label_login.setText("Your username is incorrect try again!")
            self.error_label_login.setVisible(True)
        elif response == "password":
            self.error_label_login.setText("Your password is incorrect try again!")
            self.error_label_login.setVisible(True)
        else:
            self.error_label_login.setVisible(False)
            connectedUserId = response
            save_user_id(response)

            if self.menu is None:
                self.menu = MenuWindow()
                self.close()
            else:
                self.menu.activateWindow()

    def openFirstWindow(self):
        if self.general is None:
            self.general = MainWindow1()
            self.close()
        else:
            self.general.activateWindow()



class RegisterWindow(QMainWindow):
    def __init__(self):
        super(RegisterWindow, self).__init__()

        self.menu = None
        self.general = None

        uic.loadUi("register.ui", self)

        self.submitButton_register = self.findChild(QPushButton, "submitButton_register")
        self.error_label_register = self.findChild(QLabel, "error_label_register")
        self.lineEdit_username_register = self.findChild(QLineEdit, "lineEdit_username_register")
        self.lineEdit_password_register = self.findChild(QLineEdit, "lineEdit_password_register")
        self.lineEdit_mail_register = self.findChild(QLineEdit, "lineEdit_mail_register")
        self.backButton_register = self.findChild(QPushButton, "backButton_register")


        self.error_label_register.setVisible(False)

        self.submitButton_register.clicked.connect(self.registerNewUserIntoTheDataBase)
        self.backButton_register.clicked.connect(self.openFirstWindow)

        self.show()

    def registerNewUserIntoTheDataBase(self):
        response = registerMenu(conn, self.lineEdit_username_register.text(), self.lineEdit_password_register.text(), self.lineEdit_mail_register.text())
        if response == "name":
            self.error_label_register.setVisible(True)
        else:
            self.error_label_register.setVisible(False)
            connectedUserId = response
            save_user_id(response)
            if self.menu is None:
                self.menu = MenuWindow()
                self.close()
            else:
                self.menu.activateWindow()


    def openFirstWindow(self):
        if self.general is None:
            self.general = MainWindow1()
            self.close()
        else:
            self.general.activateWindow()



class VideoWindow(QMainWindow):
    def __init__(self):
        super(VideoWindow, self).__init__()

        self.menu = None

        uic.loadUi("video.ui", self)

        self.menuButton_camera = self.findChild(QPushButton, "menuButton_camera")
        self.label_video = self.findChild(QLabel, "label_video")

        self.show()

        self.menuButton_camera.clicked.connect(self.openMenuWindow)

        user_id=get_user_id()

        displayImage(self.label_video,conn,user_id)
        """
        #self.cap = cv2.VideoCapture(0)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1000/60)  # 60 fps

    def update_frame(self):
        #ret, img = self.cap.read()
        ret, img = camera()
        if ret:
            height, width, channel = img.shape
            bytesPerLine = 3 * width
            qImg = QtGui.QImage(img.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888)
            self.label_video.setPixmap(QtGui.QPixmap.fromImage(qImg))
        else:
            print("Error reading frame from camera")

    def __del__(self):
        self.cap.release()
"""

    def openMenuWindow(self):
        if self.menu is None:
            write_stop_run()
            self.menu = MenuWindow()
            self.close()
        else:
            write_stop_run()
            self.menu.activateWindow()

    def closeEvent(self, event):
        write_stop_run()
        event.accept()



class MenuWindow(QMainWindow):
    def __init__(self):
        super(MenuWindow, self).__init__()

        self.video = None
        self.menu = None

        uic.loadUi("menu.ui", self)


        self.label_00000 = self.findChild(QLabel, "label_00000")
        self.label_10000 = self.findChild(QLabel, "label_10000")
        self.label_11000 = self.findChild(QLabel, "label_11000")
        self.label_11100 = self.findChild(QLabel, "label_11100")
        self.backButton_menu1 = self.findChild(QPushButton, "backButton_menu1")
        self.nextButton_menu1 = self.findChild(QPushButton, "nextButton_menu1")


        self.nextButton_menu1.clicked.connect(self.goToMenu1)
        self.backButton_menu1.clicked.connect(self.openVideoWindow)

        self.comboBox_00000 = self.findChild(QComboBox, "comboBox_00000")
        self.comboBox_10000 = self.findChild(QComboBox, "comboBox_10000")
        self.comboBox_11000 = self.findChild(QComboBox, "comboBox_11000")
        self.comboBox_11100 = self.findChild(QComboBox, "comboBox_11100")
        self.pushButton_00000 = self.findChild(QPushButton, "pushButton_00000")
        self.pushButton_10000 = self.findChild(QPushButton, "pushButton_10000")
        self.pushButton_11000 = self.findChild(QPushButton, "pushButton_11000")
        self.pushButton_11100 = self.findChild(QPushButton, "pushButton_11100")


        self.pushButton_00000.clicked.connect(lambda: self.putCommendInDataBase("00000"))
        self.pushButton_10000.clicked.connect(lambda: self.putCommendInDataBase("10000"))
        self.pushButton_11000.clicked.connect(lambda: self.putCommendInDataBase("11000"))
        self.pushButton_11100.clicked.connect(lambda: self.putCommendInDataBase("11100"))

        self.label_00000.setVisible(False)
        self.label_10000.setVisible(False)
        self.label_11000.setVisible(False)
        self.label_11100.setVisible(False)


        pixmap_00000 = QtGui.QPixmap("pics/00000.jpeg")
        self.label_00000.setPixmap(pixmap_00000)

        self.label_00000.setScaledContents(True)

        self.label_00000.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_10000 = QtGui.QPixmap("pics/10000.jpeg")
        self.label_10000.setPixmap(pixmap_10000)

        self.label_10000.setScaledContents(True)

        self.label_10000.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_11000 = QtGui.QPixmap("pics/11000.jpeg")
        self.label_11000.setPixmap(pixmap_11000)

        self.label_11000.setScaledContents(True)

        self.label_11000.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_11100 = QtGui.QPixmap("pics/11100.jpeg")
        self.label_11100.setPixmap(pixmap_11100)

        self.label_11100.setScaledContents(True)

        self.label_11100.setAlignment(QtCore.Qt.AlignCenter)



        self.label_00000.show()
        self.label_10000.show()
        self.label_11000.show()
        self.label_11100.show()

        self.show()

    def openVideoWindow(self):
        if self.video is None:
            write_allow_run()
            self.close()
            self.video = VideoWindow()
        else:
            write_allow_run()
            self.video.activateWindow()

    def putCommendInDataBase(self, combination):
        comboBoxName = "comboBox_" + combination
        comboBox = self.findChild(QComboBox, comboBoxName)
        if comboBox is not None:
            item_selected = comboBox.currentText()
            item_selected_without_spaces = item_selected.replace(" ", "_")
            putCommandInSpesificCombination(conn, functions_dict[item_selected_without_spaces], combination, get_user_id())


    def goToMenu1(self):
        if self.menu is None:
            self.menu = MenuWindow1()
            self.close()
        else:
            self.menu.activateWindow()



class MenuWindow1(QMainWindow):
    def __init__(self):
        super(MenuWindow1, self).__init__()

        self.video = None
        self.menu1 = None
        self.menu2 = None


        uic.loadUi("menu1.ui", self)


        self.label1_11110 = self.findChild(QLabel, "label1_11110")
        self.label1_11111 = self.findChild(QLabel, "label1_11111")
        self.label1_00001 = self.findChild(QLabel, "label1_00001")
        self.label1_00011 = self.findChild(QLabel, "label1_00011")
        self.startButton1_menu1 = self.findChild(QPushButton, "startButton1_menu1")
        self.nextButton1_menu1 = self.findChild(QPushButton, "nextButton1_menu1")
        self.prevButton1_menu1 = self.findChild(QPushButton, "prevButton1_menu1")



        self.prevButton1_menu1.clicked.connect(self.goBackToForm0)
        self.nextButton1_menu1.clicked.connect(self.goToForm2)
        self.startButton1_menu1.clicked.connect(self.openVideoWindow)

        self.comboBox1_11110 = self.findChild(QComboBox, "comboBox1_11110")
        self.comboBox1_11111 = self.findChild(QComboBox, "comboBox1_11111")
        self.comboBox1_00001 = self.findChild(QComboBox, "comboBox1_00001")
        self.comboBox1_00011 = self.findChild(QComboBox, "comboBox1_00011")
        self.pushButton1_11110 = self.findChild(QPushButton, "pushButton1_11110")
        self.pushButton1_11111 = self.findChild(QPushButton, "pushButton1_11111")
        self.pushButton1_00001 = self.findChild(QPushButton, "pushButton1_00001")
        self.pushButton1_00011 = self.findChild(QPushButton, "pushButton1_00011")


        self.pushButton1_11110.clicked.connect(lambda: self.putCommendInDataBase("11110"))
        self.pushButton1_11111.clicked.connect(lambda: self.putCommendInDataBase("11111"))
        self.pushButton1_00001.clicked.connect(lambda: self.putCommendInDataBase("00001"))
        self.pushButton1_00011.clicked.connect(lambda: self.putCommendInDataBase("00011"))

        self.label1_11110.setVisible(False)
        self.label1_11111.setVisible(False)
        self.label1_00001.setVisible(False)
        self.label1_00011.setVisible(False)


        pixmap_11110 = QtGui.QPixmap("pics/11110.jpeg")
        self.label1_11110.setPixmap(pixmap_11110)

        self.label1_11110.setScaledContents(True)

        self.label1_11110.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_11111 = QtGui.QPixmap("pics/11111.jpeg")
        self.label1_11111.setPixmap(pixmap_11111)

        self.label1_11111.setScaledContents(True)

        self.label1_11111.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_00001 = QtGui.QPixmap("pics/00001.jpeg")
        self.label1_00001.setPixmap(pixmap_00001)

        self.label1_00001.setScaledContents(True)

        self.label1_00001.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_00011 = QtGui.QPixmap("pics/00011.jpeg")
        self.label1_00011.setPixmap(pixmap_00011)

        self.label1_00011.setScaledContents(True)

        self.label1_00011.setAlignment(QtCore.Qt.AlignCenter)



        self.label1_11110.show()
        self.label1_11111.show()
        self.label1_00001.show()
        self.label1_00011.show()

        self.show()

    def openVideoWindow(self):
        if self.video is None:
            write_allow_run()
            self.close()
            self.video = VideoWindow()
        else:
            write_allow_run()
            self.video.activateWindow()

    def putCommendInDataBase(self, combination):
        comboBoxName = "comboBox1_" + combination
        comboBox = self.findChild(QComboBox, comboBoxName)
        if comboBox is not None:
            item_selected = comboBox.currentText()
            item_selected_without_spaces = item_selected.replace(" ", "_")
            putCommandInSpesificCombination(conn, functions_dict[item_selected_without_spaces], combination, get_user_id())

    def goBackToForm0(self):
        if self.menu1 is None:
            self.menu1 = MenuWindow()
            self.close()
        else:
            self.menu1.activateWindow()

    def goToForm2(self):
        if self.menu2 is None:
            self.menu2 = MenuWindow2()
            self.close()
        else:
            self.menu2.activateWindow()



class MenuWindow2(QMainWindow):
    def __init__(self):
        super(MenuWindow2, self).__init__()

        self.video = None
        self.menu1 = None
        self.menu3 = None


        uic.loadUi("menu2.ui", self)


        self.label2_00111 = self.findChild(QLabel, "label2_00111")
        self.label2_01111 = self.findChild(QLabel, "label2_01111")
        self.label2_00010 = self.findChild(QLabel, "label2_00010")
        self.label2_00110 = self.findChild(QLabel, "label2_00110")
        self.startButton2_menu2 = self.findChild(QPushButton, "startButton2_menu2")
        self.nextButton2_menu2 = self.findChild(QPushButton, "nextButton2_menu2")
        self.prevButton2_menu2 = self.findChild(QPushButton, "prevButton2_menu2")



        self.prevButton2_menu2.clicked.connect(self.goBackToForm1)
        self.nextButton2_menu2.clicked.connect(self.goToForm3)
        self.startButton2_menu2.clicked.connect(self.openVideoWindow)

        self.comboBox2_00111 = self.findChild(QComboBox, "comboBox2_00111")
        self.comboBox2_01111 = self.findChild(QComboBox, "comboBox2_01111")
        self.comboBox2_00010 = self.findChild(QComboBox, "comboBox2_00010")
        self.comboBox2_00110 = self.findChild(QComboBox, "comboBox2_00110")
        self.pushButton2_00111 = self.findChild(QPushButton, "pushButton2_00111")
        self.pushButton2_01111 = self.findChild(QPushButton, "pushButton2_01111")
        self.pushButton2_00010 = self.findChild(QPushButton, "pushButton2_00010")
        self.pushButton2_00110 = self.findChild(QPushButton, "pushButton2_00110")


        self.pushButton2_00111.clicked.connect(lambda: self.putCommendInDataBase("00111"))
        self.pushButton2_01111.clicked.connect(lambda: self.putCommendInDataBase("01111"))
        self.pushButton2_00010.clicked.connect(lambda: self.putCommendInDataBase("00010"))
        self.pushButton2_00110.clicked.connect(lambda: self.putCommendInDataBase("00110"))

        self.label2_00111.setVisible(False)
        self.label2_01111.setVisible(False)
        self.label2_00010.setVisible(False)
        self.label2_00110.setVisible(False)


        pixmap_00111 = QtGui.QPixmap("pics/00111.jpeg")
        self.label2_00111.setPixmap(pixmap_00111)

        self.label2_00111.setScaledContents(True)

        self.label2_00111.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_01111 = QtGui.QPixmap("pics/01111.jpeg")
        self.label2_01111.setPixmap(pixmap_01111)

        self.label2_01111.setScaledContents(True)

        self.label2_01111.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_00010 = QtGui.QPixmap("pics/00010.jpeg")
        self.label2_00010.setPixmap(pixmap_00010)

        self.label2_00010.setScaledContents(True)

        self.label2_00010.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_00110 = QtGui.QPixmap("pics/00110.jpeg")
        self.label2_00110.setPixmap(pixmap_00110)

        self.label2_00110.setScaledContents(True)

        self.label2_00110.setAlignment(QtCore.Qt.AlignCenter)



        self.label2_00111.show()
        self.label2_01111.show()
        self.label2_00010.show()
        self.label2_00110.show()

        self.show()

    def openVideoWindow(self):
        if self.video is None:
            write_allow_run()
            self.close()
            self.video = VideoWindow()
        else:
            write_allow_run()
            self.video.activateWindow()

    def putCommendInDataBase(self, combination):
        comboBoxName = "comboBox2_" + combination
        comboBox = self.findChild(QComboBox, comboBoxName)
        if comboBox is not None:
            item_selected = comboBox.currentText()
            item_selected_without_spaces = item_selected.replace(" ", "_")
            putCommandInSpesificCombination(conn, functions_dict[item_selected_without_spaces], combination, get_user_id())

    def goBackToForm1(self):
        if self.menu1 is None:
            self.menu1 = MenuWindow1()
            self.close()
        else:
            self.menu1.activateWindow()

    def goToForm3(self):
        if self.menu3 is None:
            self.menu3 = MenuWindow3()
            self.close()
        else:
            self.menu3.activateWindow()



class MenuWindow3(QMainWindow):
    def __init__(self):
        super(MenuWindow3, self).__init__()

        self.video = None
        self.menu2 = None
        self.menu4 = None


        uic.loadUi("menu3.ui", self)


        self.label3_01110 = self.findChild(QLabel, "label3_01110")
        self.label3_10011 = self.findChild(QLabel, "label3_10011")
        self.label3_10001 = self.findChild(QLabel, "label3_10001")
        self.label3_01100 = self.findChild(QLabel, "label3_01100")
        self.startButton3_menu3 = self.findChild(QPushButton, "startButton3_menu3")
        self.nextButton3_menu3 = self.findChild(QPushButton, "nextButton3_menu3")
        self.prevButton3_menu3 = self.findChild(QPushButton, "prevButton3_menu3")



        self.prevButton3_menu3.clicked.connect(self.goBackToForm2)
        self.nextButton3_menu3.clicked.connect(self.goToForm4)
        self.startButton3_menu3.clicked.connect(self.openVideoWindow)

        self.comboBox3_01110 = self.findChild(QComboBox, "comboBox3_01110")
        self.comboBox3_10011 = self.findChild(QComboBox, "comboBox3_10011")
        self.comboBox3_10001 = self.findChild(QComboBox, "comboBox3_10001")
        self.comboBox3_01100 = self.findChild(QComboBox, "comboBox3_01100")
        self.pushButton3_01110 = self.findChild(QPushButton, "pushButton3_01110")
        self.pushButton3_10011 = self.findChild(QPushButton, "pushButton3_10011")
        self.pushButton3_10001 = self.findChild(QPushButton, "pushButton3_10001")
        self.pushButton3_01100 = self.findChild(QPushButton, "pushButton3_01100")


        self.pushButton3_01110.clicked.connect(lambda: self.putCommendInDataBase("01110"))
        self.pushButton3_10011.clicked.connect(lambda: self.putCommendInDataBase("10011"))
        self.pushButton3_10001.clicked.connect(lambda: self.putCommendInDataBase("10001"))
        self.pushButton3_01100.clicked.connect(lambda: self.putCommendInDataBase("01100"))

        self.label3_01110.setVisible(False)
        self.label3_10011.setVisible(False)
        self.label3_10001.setVisible(False)
        self.label3_01100.setVisible(False)


        pixmap_01110 = QtGui.QPixmap("pics/01110.jpeg")
        self.label3_01110.setPixmap(pixmap_01110)

        self.label3_01110.setScaledContents(True)

        self.label3_01110.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_10011 = QtGui.QPixmap("pics/10011.jpeg")
        self.label3_10011.setPixmap(pixmap_10011)

        self.label3_10011.setScaledContents(True)

        self.label3_10011.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_10001 = QtGui.QPixmap("pics/10001.jpeg")
        self.label3_10001.setPixmap(pixmap_10001)

        self.label3_10001.setScaledContents(True)

        self.label3_10001.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_01100 = QtGui.QPixmap("pics/01100.jpeg")
        self.label3_01100.setPixmap(pixmap_01100)

        self.label3_01100.setScaledContents(True)

        self.label3_01100.setAlignment(QtCore.Qt.AlignCenter)



        self.label3_01110.show()
        self.label3_10011.show()
        self.label3_10001.show()
        self.label3_01100.show()

        self.show()

    def openVideoWindow(self):
        if self.video is None:
            write_allow_run()
            self.close()
            self.video = VideoWindow()
        else:
            write_allow_run()
            self.video.activateWindow()

    def putCommendInDataBase(self, combination):
        comboBoxName = "comboBox3_" + combination
        comboBox = self.findChild(QComboBox, comboBoxName)
        if comboBox is not None:
            item_selected = comboBox.currentText()
            item_selected_without_spaces = item_selected.replace(" ", "_")
            putCommandInSpesificCombination(conn, functions_dict[item_selected_without_spaces], combination, get_user_id())

    def goBackToForm2(self):
        if self.menu2 is None:
            self.menu2 = MenuWindow2()
            self.close()
        else:
            self.menu2.activateWindow()

    def goToForm4(self):
        if self.menu4 is None:
            self.menu4 = MenuWindow4()
            self.close()
        else:
            self.menu4.activateWindow()



class MenuWindow4(QMainWindow):
    def __init__(self):
        super(MenuWindow4, self).__init__()

        self.video = None
        self.menu3 = None


        uic.loadUi("menu4.ui", self)


        self.label4_10010 = self.findChild(QLabel, "label4_10010")
        self.label4_10110 = self.findChild(QLabel, "label4_10110")
        self.label4_10111 = self.findChild(QLabel, "label4_10111")
        self.label4_01000 = self.findChild(QLabel, "label4_01000")
        self.startButton4_menu4 = self.findChild(QPushButton, "startButton4_menu4")
        self.prevButton4_menu4 = self.findChild(QPushButton, "prevButton4_menu4")



        self.prevButton4_menu4.clicked.connect(self.goBackToForm3)
        self.startButton4_menu4.clicked.connect(self.openVideoWindow)

        self.comboBox4_10010 = self.findChild(QComboBox, "comboBox4_10010")
        self.comboBox4_10110 = self.findChild(QComboBox, "comboBox4_10110")
        self.comboBox4_10111 = self.findChild(QComboBox, "comboBox4_10111")
        self.comboBox4_01000 = self.findChild(QComboBox, "comboBox4_01000")
        self.pushButton4_10010 = self.findChild(QPushButton, "pushButton4_10010")
        self.pushButton4_10110 = self.findChild(QPushButton, "pushButton4_10110")
        self.pushButton4_10111 = self.findChild(QPushButton, "pushButton4_10111")
        self.pushButton4_01000 = self.findChild(QPushButton, "pushButton4_01000")


        self.pushButton4_10010.clicked.connect(lambda: self.putCommendInDataBase("10010"))
        self.pushButton4_10110.clicked.connect(lambda: self.putCommendInDataBase("10110"))
        self.pushButton4_10111.clicked.connect(lambda: self.putCommendInDataBase("10111"))
        self.pushButton4_01000.clicked.connect(lambda: self.putCommendInDataBase("01000"))

        self.label4_10010.setVisible(False)
        self.label4_10110.setVisible(False)
        self.label4_10111.setVisible(False)
        self.label4_01000.setVisible(False)


        pixmap_10010 = QtGui.QPixmap("pics/10010.jpeg")
        self.label4_10010.setPixmap(pixmap_10010)

        self.label4_10010.setScaledContents(True)

        self.label4_10010.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_10110 = QtGui.QPixmap("pics/10110.jpeg")
        self.label4_10110.setPixmap(pixmap_10110)

        self.label4_10110.setScaledContents(True)

        self.label4_10110.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_10111 = QtGui.QPixmap("pics/10111.jpeg")
        self.label4_10111.setPixmap(pixmap_10111)

        self.label4_10111.setScaledContents(True)

        self.label4_10111.setAlignment(QtCore.Qt.AlignCenter)

        pixmap_01000 = QtGui.QPixmap("pics/01000.jpeg")
        self.label4_01000.setPixmap(pixmap_01000)

        self.label4_01000.setScaledContents(True)

        self.label4_01000.setAlignment(QtCore.Qt.AlignCenter)



        self.label4_10010.show()
        self.label4_10110.show()
        self.label4_10111.show()
        self.label4_01000.show()

        self.show()

    def openVideoWindow(self):
        if self.video is None:
            write_allow_run()
            self.close()
            self.video = VideoWindow()
        else:
            write_allow_run()
            self.video.activateWindow()

    def putCommendInDataBase(self, combination):
        comboBoxName = "comboBox4_" + combination
        comboBox = self.findChild(QComboBox, comboBoxName)
        if comboBox is not None:
            item_selected = comboBox.currentText()
            item_selected_without_spaces = item_selected.replace(" ", "_")
            putCommandInSpesificCombination(conn, functions_dict[item_selected_without_spaces], combination, get_user_id())

    def goBackToForm3(self):
        if self.menu3 is None:
            self.menu3 = MenuWindow3()
            self.close()
        else:
            self.menu3.activateWindow()



conn = openDataBase()   #open data base or open her and gets the conn to the db
app = QApplication(sys.argv)
userMenu = MainWindow1()
#userMenu = VideoWindow()
#menu = MenuWindow()
sys.exit(app.exec_())
