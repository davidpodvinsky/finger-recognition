import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def openDataBase():
    database = "handRecognitaion.db"

    sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                        id integer PRIMARY KEY,
                                        userName text NOT NULL,
                                        password text NOT NULL,
                                        mail text NOT NULL
                                    ); """

    sql_create_commands_table = """ CREATE TABLE IF NOT EXISTS commands (
                                    id integer PRIMARY KEY,
                                    user_id integer NOT NULL,
                                    "00000" text, "01000" text, "00100" text, "01100" text, "00010" text, "01010" text, "00110" text, "01110" text, "10000" text, "11000" text, "10100" text, "11100" text, "10010" text, "11010" text, "10110" text, "11110" text, "00001" text, "01001" text, "00101" text, "01101" text, "00011" text, "01011" text, "00111" text, "01111" text, "10001" text, "11001" text, "10101" text, "11101" text, "10011" text, "11011" text, "10111" text, "11111" text, 
                                    FOREIGN KEY (user_id) REFERENCES users (id)
                                );"""

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create users table
        create_table(conn, sql_create_users_table)
        conn.commit()   # commit the changes before doing anything else on the db
        # create commands table
        create_table(conn, sql_create_commands_table)
        conn.commit()   # commit the changes before doing anything else on the db
    else:
        print("Error! cannot create the database connection.")

    return conn


def getNewId(conn):
    cursor = conn.cursor()
    sql_query = """SELECT id FROM users ORDER BY id DESC LIMIT 1;"""
    cursor.execute(sql_query)
    result = cursor.fetchone()
    if result:
        cursor.close()
        return result[0] + 1
    else:
        return 1



def registerUser(userName, password, mail, conn):
    newId = getNewId(conn)
    sql_query = """SELECT * FROM users WHERE userName=?"""
    data_tuple_find_user = (userName,)
    sql_insert_query_with_parm = """INSERT INTO users
                       (id, userName, password, mail)
                       VALUES
                       (?, ?, ?, ?);"""
    sql_insert_query_with_parm_1 = """INSERT INTO commands
                       (id, user_id)
                       VALUES
                       (?, ?);"""
    data_tuple = (newId, userName, password, mail)
    data_tuple_1 = (newId, newId)
    cursor = conn.cursor()
    cursor.execute(sql_query, data_tuple_find_user)
    result = cursor.fetchall()
    if result:
        cursor.close()
        return "name"
    else:
        cursor.execute(sql_insert_query_with_parm, data_tuple)
        cursor.execute(sql_insert_query_with_parm_1, data_tuple_1)
        conn.commit()
        cursor.close()
    return newId    #return the user id that is connected now -> after register auto connect


def loginUser(userName, password, conn):
    cursor = conn.cursor()
    sql_query = """SELECT * FROM users WHERE userName=?"""
    data_tuple = (userName,)
    cursor.execute(sql_query, data_tuple)
    result = cursor.fetchall()
    if result:
        if result[0][2] == password:    # check if the password from the db matches the password user entered
            cursor.close()
            return result[0][0]     # return the id of user who connected to the system
        else:
            cursor.close()
            return "password"   # return the word password if the name is found but password doesnt matches
    else:
        cursor.close()
        return "name"   # return the word name if the name is not found


def putCommandInSpesificCombination(conn, command, combination, user_id):
    cursor = conn.cursor()
    sql = """UPDATE commands 
    SET '""" + combination + """'= ? 
    WHERE user_id = ?"""
    data_tuple = (command, user_id)
    cursor.execute(sql, data_tuple)
    conn.commit()
    cursor.close()



def get_command_from_specific_combination(conn, combination, user_id):
    cursor = conn.cursor()
    #sql_query = """SELECT * FROM commands WHERE user_id=?"""
    sql_query = """SELECT "{}" FROM commands WHERE user_id=?""".format(combination)
    data_tuple = (user_id, )
    cursor.execute(sql_query, data_tuple)
    result = cursor.fetchall()
    return result[0][0]


