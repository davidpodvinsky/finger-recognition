from dataBase import create_connection, openDataBase, registerUser, loginUser, putCommandInSpesificCombination
import os

# the function below uses the gui

def loginMenu(conn, username, password):
    result = loginUser(username, password, conn)
    if result == "name":
        return "name"
    elif result == "password":
        return "password"
    else:
        return result



def registerMenu(conn, username, password,email):
    result = registerUser(username, password, email, conn)
    if result == "name":
        return "name"
    else:
        return result





#the functions below uses the cmd

"""
def loginMenu(conn):
    valid = 0
    while valid != 1:
        username = input("Enter username: ")
        password = input("Enter password: ")
        result = loginUser(username, password, conn)
        if result == "name":
            print("Your username is incorrect try again!")
        elif result == "password":
            print("Your password is incorrect try again!")
        else:
            return result
"""

"""
def registerMenu(conn):
    username = input("Enter username: ")
    password = input("Enter password: ")
    email = input("Enter email: ")
    result = registerUser(username, password, email, conn)
    return result

"""

# no need for this function at the moment maybe we will use her later
"""
def userMenu(conn):
    choice = '3'
    while choice != '1' or choice != '2':
        print("Enter your choice!")
        print("1: LOGIN\n2: REGISTER")
        choice = input("Choice: ")
        if choice == '1':
            break
        elif choice == '2':
            break
        else:
            print("Invalid input try again!")

    if choice == '1':   # in case user want to login
        return loginMenu(conn)
    else:   # in case user want to register
        return registerMenu(conn)
"""

# no need for this function at the moment maybe we will use her later
"""
def combinationsMenu(conn, user_id):
    choice = '3'
    while choice != '1' or choice != '2':
        print("Enter your choice!")
        print("1: 5 fingers\n2: 4 fingers\n3: 3 fingers\n4: 2 fingers\n5: 1 fingers\n6: 0 fingers\n 0: exit")
        choice = input("Choice: ")
        if choice == '1':
            command = input("enter 1:chrome\n2: notepad\n3: excel\n4: word")
            if command == '1':
                #putCommandInSpesificCombination(conn, 'os.system("chrome")', '11111', user_id)
                putCommandInSpesificCombination(conn, 'print(1)', '11111', user_id)
            if command == '2':
                pass
            if command == '3':
                pass
            if command == '4':
                pass
        elif choice == '2':
            pass
        elif choice == '3':
            pass
        elif choice == '4':
            pass
        elif choice == '5':
            pass
        elif choice == '6':
            pass
        elif choice == '0':
            break
        else:
            print("Invalid input try again!")
"""
