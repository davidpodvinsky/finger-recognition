A copy of a completed project made in senior year in magshimim.
Had to copy the final product due to complications with making it public.

About the project:
A hand-recognizing software that can recognize which finger combination the user is showing, and run a predefined command from a database according to what the user decided. 
The project uses an AI to find the palm, which allows us to calculate what fingers the user is showing.
The project features a GUI made with PyQt5 that is connected to a database that connects for every user, a command to a combination.

How to run:
Install a 64-bit Python (preferably Python 3.8), apply it as the interpreter, and run "gui.py".
A window will show up allowing you to either login or register, after that you will be shown a menu, in the menu you can see pictures showing the combinations, and a list from where you can choose what command you want to run, you then click submit and you are free to show you fingers to the camera. Note that for the commands to run you need to show the same combination for around 2 seconds.
Although the menu suggests you can open a file, we didn't add this part because we thought it was too simple to implement and therefore not worth our time.

Please don't be bothered by the names of the files:
"num3.py" is the file that contains the image processing code, the file is responsible for identifying what fingers are shown and making sure they're shown for a continuous period to ensure the user's intention.
"meth.py" contains the mathematical part of the project, such as rotating points around a point or calculating the angle by which the hand is rotated.
"imageProces.py" performs some operations on the image such as improving the video quality.
"database.py" as the name suggests does everything related to the database.
"menu.py" and "gui.py" contains the GUI parts, and "gui.py" is the main file that is starting the project piecing everything together.
"functionsAsStrings.py" contains all the possible commands

The database is made from 2 tables, a table that contains user data, and a table that contains the commands.
The commands table is made from the user id, and 32 columns representing every finger combination, each column will contain the command to run when the combination is shown. The command is stored as Python code that will be executed via the '_exec()' command.