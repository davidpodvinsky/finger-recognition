import math
from math import radians, sin, cos
import numpy as np

'''
calculats the degrees between the current hand state and the x axis
gets: array of points of landmarks, the direction of the palm
'''
def calc_degrees(handLandmarks,handLabel):
  line1Y2=handLandmarks[17][1]
  line1Y1=handLandmarks[5][1]

  line1X2=handLandmarks[17][0]
  line1X1=handLandmarks[5][0]

  angle1=0
  if handLabel=="Left":
    angle1=math.atan2(line1Y1-line1Y2,line1X1-line1X2)
  else:
    angle1=math.atan2(line1Y2-line1Y1,line1X2-line1X1)

  angleDegrees = (angle1) * 360 / (2*math.pi)

  return angleDegrees



def calc_center_of_palm(handLandmarks):
  total_x=handLandmarks[17][0]+handLandmarks[1][0]
  total_y=handLandmarks[0][1]+handLandmarks[9][1]

  return(total_x/2,total_y/2)


def rotate_point_around_center(point_to_be_rotated, angle, center_point):
    angle = radians(angle)

    xnew = cos(angle) * (point_to_be_rotated[0] - center_point[0]) - sin(angle) * (point_to_be_rotated[1] - center_point[1]) + center_point[0]
    ynew = sin(angle) * (point_to_be_rotated[0] - center_point[0]) + cos(angle) * (point_to_be_rotated[1] - center_point[1]) + center_point[1]

    return (round(xnew,2),round(ynew,2))


